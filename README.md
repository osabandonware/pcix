No claim to original works.

All submissions must be compressed in 7z format in file size of less than 24MB.

> 7z -v23M a file.7z os.iso mx=9m

Source code may be in .tar or .tar.gz.

A lot of these came from WinWorld.com.